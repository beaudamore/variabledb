﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendornetVariableDB.Models;
using System.Linq;

namespace VendornetVariableDBTest
{
    [TestClass]
    public class VariableDBTest
    {
        private static ExtVariablesEntities _varDB;

        VariableDBTest()
        {
            _varDB = new ExtVariablesEntities();
        }

        [TestMethod]
        public void ReadData()
        {
            try
            {
                var getAccounts = (from a in _varDB.Accounts
                                   select a).ToList();
                Assert.IsNotNull(getAccounts);
            }
            catch(Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void CreateData()
        {
            try
            {
                Account newAccount = new Account();
                newAccount.AccountName = "TestAccount";
                _varDB.Accounts.Add(newAccount);
                _varDB.SaveChanges();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}
