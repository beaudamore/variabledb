﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VendornetVariableDB.Models
{
    public class TierInfo
    {
        public int ServerAccountId { get; set; }
        public int ProcessId { get; set; }
        public int ProgramId { get; set; }
        public int AccountId { get; set; }
        public int ServerId { get; set; }
        public string ProcessName { get; set; }
        public string ProgramName { get; set; }
        public string AccountName { get; set; }
        public string ServerName { get; set; }
    }
}