﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendornetVariableDB.Models
{
    public class UpdateSetupObjList
    {
        public List<UpdateSetupObj> data { get; set; }
    }
    public class UpdateSetupObj
    {
        public string ExeName { get; set; }
        public string TFSPath { get; set; }
        public string Name { get; set; }
        public int SortIndex { get; set; }
        public int PKeyId { get; set; }
        public byte AccountTypeId { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public string Owner { get; set; }
        public int ProcessId { get; set; }
        public int ProgramId { get; set; }
        public int AccountId { get; set; }
        public int ServerId { get; set; }
        public int VariableId { get; set; }
        public byte DefLevel { get; set; }
        public bool AvailServer { get; set; }
        public bool AvailAccount { get; set; }
        public bool AvailProgram { get; set; }
        public bool AvailProcess { get; set; }
    }
}