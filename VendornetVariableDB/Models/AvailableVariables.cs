﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendornetVariableDB.Models
{
    public class AvailableVariables : IEquatable<GetProcessVariables_Result>
    {
        public AvailableVariables(Variable v, string defaultValue, bool required)
        {
            VariableId = v.VariableId;
            VariableName = v.VariableName;
            VariableDescription = v.VariableDescription;
            DefaultValue = defaultValue;
            Required = required;
        }
        public int VariableId { get; set; }
        public string VariableName { get; set; }
        public string VariableDescription { get; set; }
        public string DefaultValue { get; set; }
        public bool Required { get; set; }

        public bool Equals(GetProcessVariables_Result other)
        {
            if (this.VariableId == other.VariableId)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}