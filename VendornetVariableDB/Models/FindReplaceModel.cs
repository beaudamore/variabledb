﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendornetVariableDB.Models
{
    public class FindReplaceModel
    {
        public FindReplaceModel(string findVar, string replaceVar)
        {
            FindVar = findVar.Replace("\\", "\\\\");
            ReplaceVar = replaceVar.Replace("\\","\\\\");
        }
        public string FindVar { get; set; }
        public string ReplaceVar { get; set; }
    }
}