﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendornetVariableDB.Models
{
    public class NamedAccountProcessVariableRunning
    {
        private bool required;
        public NamedAccountProcessVariableRunning(GetVariablesByIdWDetails_Result gvidd)
        {
            VariableId = (int)gvidd.VariableId;
            VariableName = gvidd.VariableName;
            VariableData = gvidd.VarData;
            ActualVariableData = gvidd.ActualVarData;
            Tier = gvidd.Tier;
            ServerLevelData = gvidd.ServerLevelData;
            AccountLevelData = gvidd.AccountLevelData;
            ProgramLevelData = gvidd.ProgramLevelData;
            ProgramProcessLevelData = gvidd.ProgramProcessLevelData;
            VariableDescription = gvidd.VariableDescription;
            Required = bool.TryParse(gvidd.Required.ToString(), out required) && required;
            DefaultValue = gvidd.DefaultValue;
        }
        public int VariableId { get; set; }
        public string VariableName { get; set; }
        public string VariableData { get; set; }
        public string ActualVariableData { get; set; }
        public string Tier { get; set; }
        public string ServerLevelData { get; set; }
        public string AccountLevelData { get; set; }
        public string ProgramLevelData { get; set; }
        public string ProgramProcessLevelData { get; set; }
        public string VariableDescription { get; set; }
        public bool Required { get; set; }
        public string DefaultValue { get; set; }
    }
}