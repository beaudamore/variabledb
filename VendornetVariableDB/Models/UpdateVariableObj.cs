﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendornetVariableDB.Models
{
    public class UpdateVariableObjList
    {
        public List<UpdateVariableObj> data { get; set; }
    }
    public class UpdateVariableObj
    {
        public string Name { get; set; }
        public int PKeyId { get; set; }
        public string Data { get; set; }
        public int ServerAccountId { get; set; }
        public int ProcessId { get; set; }
        public int ProgramId { get; set; }
        public int ServerId { get; set; }
        public int VariableId { get; set; }
    }
}