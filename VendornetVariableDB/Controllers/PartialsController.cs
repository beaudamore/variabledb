﻿using System.Web.Mvc;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.Controllers
{
    [ChildActionOnly]
    public class PartialsController : Controller
    {
        public ActionResult TierLocation(TierInfo tierInfo)
        {
            return PartialView("_TierLocation", tierInfo);
        }
    }
}