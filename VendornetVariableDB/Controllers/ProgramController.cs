﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VendornetVariableDB.Models;
using System.Web.Script.Serialization;

using VendornetVariableDB.ViewModels;
using VendornetVariableDB.Classes;

namespace VendornetVariableDB.Controllers
{
    [Authorize]
    public class ProgramController : Controller
    {
        private ExtVariablesEntities _varDB = new ExtVariablesEntities();

        public ActionResult Index(int serverAccountId)
        {           
            // possible to do these below in 1 command? same record in table (ServerAccounts)
            int serverId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.ServerId).First();
            int accountId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.AccountId).First();

            var returnModel = new ProgramIndexViewModel()
            {
                TierInfo = new TierInfo
                {
                    ServerAccountId = serverAccountId,
                    ServerId = serverId,
                    ServerName = (from s in _varDB.Servers where s.ServerId == serverId select s.ServerName).First().ToString(),
                    AccountId = accountId,
                    AccountName = (from a in _varDB.Accounts where a.AccountId == accountId select a.AccountName).First().ToString(),
                },
                AllPrograms = (from p in _varDB.Programs select p).Distinct().ToList()
            };

            return View(returnModel);
        }

        public ActionResult Modify(int serverAccountId, int programId)
        {
            List<GetProgramVariables_Result> accountProgramVariables = (from apv in _varDB.GetProgramVariables(serverAccountId, programId) select apv).ToList();

            if (accountProgramVariables.Count == 0)
            {
                throw new Exception("Variables Not Set Up");
            }

            var replacements = (from r in _varDB.FindReplaces
                                where !r.Find.Contains("Process")
                                select new { r.Find, r.ReplaceWith }).ToList();

            List<GetProgramVariables_Result> currentVars = new List<GetProgramVariables_Result>();
            List<GetProgramVariables_Result> addRecommendedVars = new List<GetProgramVariables_Result>();
            List<GetProgramVariables_Result> addNonRecommendedVars = new List<GetProgramVariables_Result>();

            foreach (GetProgramVariables_Result accountProgramVariable in accountProgramVariables)
            {
                if (accountProgramVariable.AccountProgramVariableId != null)
                {
                    // AccountProgramVariables.AccountProgramVariableId is the primary key and it has a value; this value is already set and can be modified
                    currentVars.Add(accountProgramVariable);
                }
                else
                {
                    // AccountProgramVariables.AccountProgramVariableId is the primary key and it is null;
                    //    this variable does not exist at this level for this server/account/program/Program
                    if (accountProgramVariable.DefaultLevel == (byte)Enums.VariableLevelEnum.Program)
                    {
                        // DefaultLevel 3 means this variable is recommended to be set at the Program level
                        addRecommendedVars.Add(accountProgramVariable);
                    }
                    else
                    {
                        // It is not recommended that this variable be set at the Program level; however, it is allowed to be set at that level
                        addNonRecommendedVars.Add(accountProgramVariable);
                    }
                }
            }

            List<FindReplaceModel> findReplacements = new List<FindReplaceModel>();

            foreach (var replacement in replacements)
            {
                findReplacements.Add(new FindReplaceModel(replacement.Find, replacement.ReplaceWith));
            }

            // does not work, even when using public class AvailableVariables : IEquatable<GetProgramVariables_Result>
            //availableVariables = availableVariables.RemoveAll(item => accountProgramVariables.Contains(item));

            // possible to do these below in 1 command? same record in table (ServerAccounts)
            int serverId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.ServerId).First();
            int accountId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.AccountId).First();

            TierInfo tierInfo = new TierInfo
            {
                ServerAccountId = serverAccountId,
                ServerId = serverId,
                ServerName = accountProgramVariables.First().ServerName,
                AccountId = accountId,
                AccountName = accountProgramVariables.First().AccountName,
                ProgramId = programId,
                ProgramName = accountProgramVariables.First().ProgramName,
            };

            ProgramModifyViewModel model = new ProgramModifyViewModel()
            {
                TierInfo = tierInfo,
                CurrentVariables = currentVars,
                AddRecommendedVars = addRecommendedVars,
                AddNonRecommendedVars = addNonRecommendedVars,
                Replacements = findReplacements
            };

            return View(model);
        }

        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult AddAccountProgramVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);

            try
            {
                AccountProgramVariable dataToAdd = new AccountProgramVariable()
                {
                    ServerAccountId = updates.ServerAccountId,
                    ProgramId = updates.ProgramId,
                    VariableId = updates.VariableId,
                    VariableData = updates.Data,
                    User = User.Identity.Name
                };

                _varDB.AccountProgramVariables.Add(dataToAdd);
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Program", "Controller");

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult DeleteAccountProgramVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);
            AccountProgramVariable delThis = _varDB.AccountProgramVariables.Find(updates.PKeyId);

            try
            {
                _varDB.AccountProgramVariables.Remove(delThis);
                _varDB.SaveChanges();
                return Json("Successfully deleted " + updates.Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult UpdateAccountProgramVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);

            AccountProgramVariable variableToChange = _varDB.AccountProgramVariables.Find(updates.PKeyId);

            variableToChange.VariableData = updates.Data;
            variableToChange.User = User.Identity.Name;

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed " + updates.Name + " to " + updates.Data + ".", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult AddAccountProgramVariables(string values)
        {
            UpdateVariableObjList updates = new JavaScriptSerializer().Deserialize<UpdateVariableObjList>(values);

            try
            {
                foreach (var update in updates.data)
                {
                    _varDB.AccountProgramVariables.Add(new AccountProgramVariable()
                    {
                        ServerAccountId = update.ServerAccountId,
                        ProgramId = update.ProgramId,
                        VariableId = update.VariableId,
                        VariableData = update.Data,
                        User = User.Identity.Name
                    });
                }
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Program", "Controller");
                //ModelState.Clear();
                //return View(new Modify)

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult UpdateAccountProgramVariables(string values)
        {
            UpdateVariableObjList updates = new JavaScriptSerializer().Deserialize<UpdateVariableObjList>(values);

            List<int> variableIds = new List<int>();
            foreach (var update in updates.data)
            {
                variableIds.Add(update.PKeyId);
            }

            List<AccountProgramVariable> variableToChange = (from apv in _varDB.AccountProgramVariables
                                                             where variableIds.Contains(apv.AccountProgramVariableId)
                                                             select apv).OrderBy(apvl => apvl.AccountProgramVariableId).ToList();

            List<UpdateVariableObj> updatesList = updates.data.OrderBy(ul => ul.PKeyId).ToList();

            for (int n = 0; n < updatesList.Count; n++)
            {
                variableToChange[n].VariableData = updatesList[n].Data;
                variableToChange[n].User = User.Identity.Name;
            }

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed.", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed: " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet, Authorize(Roles = "Admin,User")]
        public ActionResult GetVariableCountBool(int serverAccountId, int programId)
        {
            var returnBool = false;
            if (_varDB.GetProgramVariables(serverAccountId, programId).Count() > 0)
            {
                returnBool = true;
            }
            return Json(returnBool, JsonRequestBehavior.AllowGet);
        }

    }
}