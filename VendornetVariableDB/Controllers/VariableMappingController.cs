﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using VendornetVariableDB.Models;
using VendornetVariableDB.ViewModels;

namespace VendornetVariableDB.Controllers
{
    [Authorize]
    public class VariableMappingController : Controller
    {
        private ExtVariablesEntities db = new ExtVariablesEntities();

        // GET: ProgramVariableMaps
        public ActionResult Index()
        {
            // get all variables to choose from to add also
            var model = new ProgramVariableMapIndexViewModel()
            {
                Programs = db.Programs.Select(x => new SelectListItem { Text = x.ProgramName, Value = x.ProgramId.ToString() })
            };          
            return View(model);
        }

        public ActionResult GetMappingsForProgramOrItemsForDdlProcesses(int id)
        {
            var test = db.Processes.Count(x => x.ProgramId == id);

            if (db.Processes.Count(x=>x.ProgramId == id) == 0)
            {
                // it has ZERO processes, so return partial view for its variables at teh program level
                ProgramVariableMixedResultViewModel model = new ProgramVariableMixedResultViewModel
                {
                    IsSuccess = true,
                    ReturnType = 1,
                    ReturnHtml = RenderPartialView("_ProgramVariableMappingList", GetProgramVariableMappingsModelByProgramId(id))
                };
                return Json(model);
            }
            else
            {
                // it doesnt have processes, return selectlistitems for second ddl (processes)
                var availableVariables = db.Processes.Where(x => x.ProgramId == id)
                                                     .Select(x => new SelectListItem { Text = x.ProcessName.Trim(), Value = x.ProcessId.ToString() })
                                                     .ToList();
                ProgramVariableMixedResultViewModel model = new ProgramVariableMixedResultViewModel
                {
                    IsSuccess = true,
                    ReturnType = 2,
                    ReturnDDLOptions = availableVariables
                };
                return Json(model);
            }
            //return View();
        }
        
        public ProgramVariableMapsToReturnViewModel GetProgramVariableMappingsModelByProgramId(int id)
        {
            // conditional where clause in case they didn't select both ddls
            var existingProgramVariableMaps = db.ProgramVariableMaps.Where(x => x.ProgramId == id);

            // get available variables for this combination (none that are already chosen)
            var availableVariables = db.Variables.Where(x => !existingProgramVariableMaps.Any(x2 => x2.VariableId == x.VariableId))
                                                    .Select(x => new SelectListItem { Text = x.VariableName.Trim(), Value = x.VariableId.ToString() });        
            // build return model
            var returnModel = new ProgramVariableMapsToReturnViewModel
            {
                AvailableVariables = availableVariables,
                ExistingProgramVariableMaps = existingProgramVariableMaps
            };

            return returnModel;// PartialView("_ProgramVariableMappingList", returnModel);
        }

        public PartialViewResult GetProcessVariableMappings(VariableMapsToGetViewModel model)
        {          
            // conditional where clause in case they didn't select both ddls
            var existingProcessVariableMaps = (from pvm in db.ProcessVariableMaps
                                           where ((string.IsNullOrEmpty(model.ProcessId) ? false : pvm.ProcessId.ToString() == model.ProcessId))
                                           select pvm).AsEnumerable();

            // get available variables for this combination (none that are already chosen)
            var availableVariables = db.Variables.Where(x => !existingProcessVariableMaps.Any(x2 => x2.VariableId == x.VariableId))
                                                    .Select(x => new SelectListItem { Text = x.VariableName, Value = x.VariableId.ToString() });

            // build return model
            var returnModel = new ProcessVariableMapsToReturnViewModel
            {
                AvailableVariables = availableVariables,
                ExistingProcessVariableMaps = existingProcessVariableMaps
            };

            return PartialView("_ProcessVariableMappingList", returnModel);
        }
        
        [Authorize(Roles = "Admin")]
        public ActionResult AddProgramVariableMap(ProgramVariableMapToAddViewModel model)
        {
            // setup return model
            JsonResultViewModel returnModel = new JsonResultViewModel
            {
                IsSuccess = true
            };

            // remove all mappings first insead of checking which ones to remove and add and blah....
            db.ProgramVariableMaps.RemoveRange(db.ProgramVariableMaps.Where(x => x.ProgramId == model.ProgramId));
            db.SaveChanges();

            foreach (var item in model.AssignedVariables)
            {
                // setup model to create
                ProgramVariableMap pvm = new ProgramVariableMap
                {
                    ProgramId = model.ProgramId,
                    VariableId = Convert.ToInt32(item.VariableId),
                    Required = Convert.ToBoolean(item.IsRequired)
                };
                // give it a shot
                try
                {
                    db.ProgramVariableMaps.Add(pvm);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    returnModel.IsSuccess = false;
                    returnModel.Message = ex.Message;
                }
            }
            return Json(returnModel);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddProcessVariableMap(ProcessVariableMapToAddViewModel model)
        {
            // setup return model
            JsonResultViewModel returnModel = new JsonResultViewModel
            {
                IsSuccess = true
            };

            // remove all mappings first insead of checking which ones to remove and add and blah....
            db.ProcessVariableMaps.RemoveRange(db.ProcessVariableMaps.Where(x => x.ProcessId == model.ProcessId));
            db.SaveChanges();

            foreach (var item in model.AssignedVariables) {
                // setup model to create
                ProcessVariableMap pvm = new ProcessVariableMap
                {
                    ProcessId = model.ProcessId,
                    VariableId = Convert.ToInt32(item.VariableId),
                    Required = Convert.ToBoolean(item.IsRequired)
                };
                // give it a shot
                try
                {
                    db.ProcessVariableMaps.Add(pvm);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    returnModel.IsSuccess = false;
                    returnModel.Message = ex.Message;
                }
            }            
            return Json(returnModel);
        }

        // GET: ProgramVariableMaps/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProgramVariableMap programVariableMap = db.ProgramVariableMaps.Find(id);
            if (programVariableMap == null)
            {
                return HttpNotFound();
            }
            return View(programVariableMap);
        }

        // POST: ProgramVariableMaps/Delete/5
        [HttpPost, ActionName("Delete"), ValidateAntiForgeryToken, Authorize(Roles = "Admin")]        
        public ActionResult DeleteConfirmed(int id)
        {
            ProgramVariableMap programVariableMap = db.ProgramVariableMaps.Find(id);
            db.ProgramVariableMaps.Remove(programVariableMap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        private string RenderPartialView(string viewName, object model)
        {
            ViewData.Model = model;
            using (System.IO.StringWriter writer = new System.IO.StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, writer);
                viewResult.View.Render(viewContext, writer);

                return writer.GetStringBuilder().ToString();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
