﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VendornetVariableDB.Models;
using System.Web.Script.Serialization;
using VendornetVariableDB.ViewModels;
using VendornetVariableDB.Classes;

namespace VendornetVariableDB.Controllers
{
    [Authorize]// Basic can view, Users,Admin can modify
    public class ServerController : Controller
    {
        private ExtVariablesEntities _varDB = new ExtVariablesEntities();

        // GET: Server
        public ActionResult Index()
        {
            List<Server> allServers = (from s in _varDB.Servers
                                       select s).ToList();

            TierInfo tierInfo = new TierInfo();

            ViewData["TierInfo"] = tierInfo;
            ViewData["Servers"] = allServers;

            var returnModel = new ServerIndexViewModel()
            {
                TierInfo = new TierInfo(),
                AllServers = _varDB.Servers.OrderBy(x => x.ServerName).ToList()
            };

            return View(returnModel);
        }

        public ActionResult Modify(int serverId)
        {
            List<GetServerVariables_Result> serverVariables = (from gsv in _varDB.GetServerVariables(serverId) select gsv)
                                                                .OrderByDescending(x => x.SortIndex)
                                                                .ToList();

            var replacements = (from r in _varDB.FindReplaces
                                where r.Find.Contains("Server")
                                select new { r.Find, r.ReplaceWith }).ToList();

            List<GetServerVariables_Result> currentVars = new List<GetServerVariables_Result>();
            List<GetServerVariables_Result> addRecommendedVars = new List<GetServerVariables_Result>();
            List<GetServerVariables_Result> addNonRecommendedVars = new List<GetServerVariables_Result>();

            foreach (GetServerVariables_Result serverVariable in serverVariables)
            {
                if (serverVariable.ServerVariableId != null)
                {
                    // AccountAccountVariables.AccountAccountVariableId is the primary key and it has a value; this value is already set and can be modified
                    currentVars.Add(serverVariable);
                }
                else
                {
                    // AccountAccountVariables.AccountAccountVariableId is the primary key and it is null;
                    //    this variable does not exist at this level for this server/account/Account/Account
                    if (serverVariable.DefaultLevel == (byte)Enums.VariableLevelEnum.Server)
                    {
                        // DefaultLevel 1 means this variable is recommended to be set at the Account level
                        addRecommendedVars.Add(serverVariable);
                    }
                    else
                    {
                        // It is not recommended that this variable be set at the Account level; however, it is allowed to be set at that level
                        addNonRecommendedVars.Add(serverVariable);
                    }
                }
            }

            List<FindReplaceModel> findReplacements = new List<FindReplaceModel>();

            foreach (var replacement in replacements)
            {
                findReplacements.Add(new FindReplaceModel(replacement.Find, replacement.ReplaceWith));
            }

            // does not work, even when using public class AvailableVariables : IEquatable<GetAccountVariables_Result>
            //availableVariables = availableVariables.RemoveAll(item => accountProgramVariables.Contains(item));

            TierInfo tierInfo = new TierInfo
            {
                ServerId = serverId,
                ServerName = serverVariables.First().ServerName,
            };

            ServerModifyViewModel model = new ServerModifyViewModel() {
                TierInfo = tierInfo,
                CurrentVariables = currentVars,
                AddRecommendedVars = addRecommendedVars,
                AddNonRecommendedVars = addNonRecommendedVars,
                Replacements = findReplacements
            };
            
            return View(model);
        }

        [HttpPost, Authorize(Roles = "Admin,User")]        
        public ActionResult AddServerVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);

            try
            {
                ServerVariable dataToAdd = new ServerVariable()
                {
                    ServerId = updates.ServerId,
                    VariableId = updates.VariableId,
                    VariableData = updates.Data,
                    User = User.Identity.Name
                };

                _varDB.ServerVariables.Add(dataToAdd);
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Index", "Server");

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]        
        public ActionResult DeleteServerVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);
            ServerVariable delThis = _varDB.ServerVariables.Find(updates.PKeyId);

            try
            {
                _varDB.ServerVariables.Remove(delThis);
                _varDB.SaveChanges();
                return Json("Successfully deleted " + updates.Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]        
        public ActionResult UpdateServerVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);

            ServerVariable variableToChange = _varDB.ServerVariables.Find(updates.PKeyId);

            variableToChange.VariableData = updates.Data;
            variableToChange.User = User.Identity.Name;

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed " + updates.Name + " to " + updates.Data + ".", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]        
        public ActionResult AddServerVariables(string values)
        {
            UpdateVariableObjList updates = new JavaScriptSerializer().Deserialize<UpdateVariableObjList>(values);

            try
            {
                foreach (var update in updates.data)
                {
                    _varDB.ServerVariables.Add(new ServerVariable()
                    {
                        ServerId = update.ServerId,
                        VariableId = update.VariableId,
                        VariableData = update.Data,
                        User = User.Identity.Name
                    });
                }
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Server", "Controller");
                //ModelState.Clear();
                //return View(new Modify)

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]        
        public ActionResult UpdateServerVariables(string values)
        {
            UpdateVariableObjList updates = new JavaScriptSerializer().Deserialize<UpdateVariableObjList>(values);

            List<int> variableIds = new List<int>();
            foreach (var update in updates.data)
            {
                variableIds.Add(update.PKeyId);
            }

            List<ServerVariable> variableToChange = (from apv in _varDB.ServerVariables
                                                     where variableIds.Contains(apv.ServerVariableId)
                                                     select apv).OrderBy(apvl => apvl.ServerVariableId).ToList();

            List<UpdateVariableObj> updatesList = updates.data.OrderBy(ul => ul.PKeyId).ToList();

            for (int n = 0; n < updatesList.Count; n++)
            {
                variableToChange[n].VariableData = updatesList[n].Data;
                variableToChange[n].User = User.Identity.Name;
            }

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed.", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed: " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}