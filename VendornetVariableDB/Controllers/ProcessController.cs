﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VendornetVariableDB.Models;
using System.Web.Script.Serialization;
using VendornetVariableDB.Classes;
using VendornetVariableDB.ViewModels;

namespace VendornetVariableDB.Controllers
{
    [Authorize]
    public class ProcessController : Controller
    {
        private ExtVariablesEntities _varDB = new ExtVariablesEntities();

        public ActionResult Index(int serverAccountId, int programId)
        {
            List<Process> allProcesses = (from pr in _varDB.Processes
                                          where pr.ProgramId == programId
                                          select pr).ToList();

            // possible to do these below in 1 command? same record in table (ServerAccounts)
            int serverId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.ServerId).First();
            int accountId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.AccountId).First();

            TierInfo tierInfo = new TierInfo
            {
                ServerAccountId = serverAccountId,
                ServerId = serverId,
                ServerName = (from s in _varDB.Servers where s.ServerId == serverId select s.ServerName).First().ToString(),
                AccountId = accountId,
                AccountName = (from a in _varDB.Accounts where a.AccountId == accountId select a.AccountName).First().ToString(),
                ProgramId = programId,
                ProgramName = (from p in _varDB.Programs where p.ProgramId == programId select p.ProgramName).First().ToString()
            };

            ViewData["TierInfo"] = tierInfo;
            ViewData["Processes"] = allProcesses;

            return View();
        }
        
        public ActionResult Modify(int serverAccountId, int programId, int processId)
        {
            List<GetProcessVariables_Result> accountProcessVariables = (from apv in _varDB.GetProcessVariables(serverAccountId, programId, processId) select apv).ToList();

            var replacements = (from r in _varDB.FindReplaces
                                select new { r.Find, r.ReplaceWith }).ToList();

            List<GetProcessVariables_Result> currentVars = new List<GetProcessVariables_Result>();
            List<GetProcessVariables_Result> addRecommendedVars = new List<GetProcessVariables_Result>();
            List<GetProcessVariables_Result> addNonRecommendedVars = new List<GetProcessVariables_Result>();

            foreach (GetProcessVariables_Result accountProcessVariable in accountProcessVariables)
            {
                if (accountProcessVariable.AccountProcessVariableId != null)
                {
                    // AccountProcessVariables.AccountProcessVariableId is the primary key and it has a value; this value is already set and can be modified
                    currentVars.Add(accountProcessVariable);
                }
                else
                {
                    // AccountProcessVariables.AccountProcessVariableId is the primary key and it is null;
                    //    this variable does not exist at this level for this server/account/program/process
                    if (accountProcessVariable.DefaultLevel == (byte)Enums.VariableLevelEnum.Process)
                    {
                        // DefaultLevel 4 means this variable is recommended to be set at the process level
                        addRecommendedVars.Add(accountProcessVariable);
                    }
                    else
                    {
                        // It is not recommended that this variable be set at the process level; however, it is allowed to be set at that level
                        addNonRecommendedVars.Add(accountProcessVariable);
                    }
                }
            }

            List<FindReplaceModel> findReplacements = new List<FindReplaceModel>();

            foreach (var replacement in replacements)
            {
                findReplacements.Add(new FindReplaceModel(replacement.Find, replacement.ReplaceWith));
            }

            // does not work, even when using public class AvailableVariables : IEquatable<GetProcessVariables_Result>
            //availableVariables = availableVariables.RemoveAll(item => accountProcessVariables.Contains(item));

            // possible to do these below in 1 command? same record in table (ServerAccounts)
            int serverId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.ServerId).First();
            int accountId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.AccountId).First();
                        
            var returnModel = new ProcessModifyViewModel()
            {
                TierInfo = new TierInfo
                {
                    ServerAccountId = serverAccountId,
                    ServerId = serverId,
                    ServerName = accountProcessVariables.First().ServerName,
                    AccountId = accountId,
                    AccountName = accountProcessVariables.First().AccountName,
                    ProgramId = programId,
                    ProgramName = accountProcessVariables.First().ProgramName,
                    ProcessId = processId,
                    ProcessName = accountProcessVariables.First().ProcessName
                },
                AddNonRecommendedVars = addNonRecommendedVars,
                AddRecommendedVars = addRecommendedVars,
                CurrentVariables = currentVars,
                Replacements = findReplacements
            };

            return View(returnModel);
        }

        public ActionResult Running(int serverAccountId, int programId, int processId)
        {
            // possible to do these below in 1 command? same record in table (ServerAccounts)
            int serverId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.ServerId).First();
            int accountId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.AccountId).First();


            string serverName = (from s in _varDB.Servers where s.ServerId == serverId select s.ServerName).First().ToString();
            string accountName = (from a in _varDB.Accounts where a.AccountId == accountId select a.AccountName).First().ToString();
            string programName = (from p in _varDB.Programs where p.ProgramId == programId select p.ProgramName).First().ToString();
            string processName = (from pr in _varDB.Processes where pr.ProgramId == programId && pr.ProcessId == processId select pr.ProcessName).First().ToString();

            var accountProcessVariablesRunning = (from apv in _varDB.GetVariablesByIdWDetails(serverId, accountId, programId, processId, serverName, accountName, programName, processName)
                                                  select apv).ToList();

            List<NamedAccountProcessVariableRunning> namedAccountProcessVariables = new List<NamedAccountProcessVariableRunning>();

            foreach (var accountProcessVariableRunning in accountProcessVariablesRunning)
            {
                namedAccountProcessVariables.Add(new NamedAccountProcessVariableRunning(accountProcessVariableRunning));
            }

            TierInfo tierInfo = new TierInfo
            {
                ServerAccountId = serverAccountId,
                ServerId = serverId,
                ServerName = serverName,
                AccountId = accountId,
                AccountName = accountName,
                ProgramId = programId,
                ProgramName = programName,
                ProcessId = processId,
                ProcessName = processName
            };

            ViewData["TierInfo"] = tierInfo;
            ViewData["RunningVariables"] = namedAccountProcessVariables;

            return View();
        }

        [HttpPost, Authorize(Roles = "Admin,User")]        
        public ActionResult AddAccountProcessVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);

            try
            {
                AccountProcessVariable dataToAdd = new AccountProcessVariable()
                {
                    ServerAccountId = updates.ServerAccountId,
                    ProcessId = updates.ProcessId,
                    VariableId = updates.VariableId,
                    VariableData = updates.Data,
                    User = User.Identity.Name
                };

                _varDB.AccountProcessVariables.Add(dataToAdd);
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Process", "Controller");
                //ModelState.Clear();
                //return View(new Modify)

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProcessVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]        
        public ActionResult DeleteAccountProcessVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);
            AccountProcessVariable delThis = _varDB.AccountProcessVariables.Find(updates.PKeyId);

            try
            {
                _varDB.AccountProcessVariables.Remove(delThis);
                _varDB.SaveChanges();
                return Json("Successfully deleted " + updates.Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]        
        public ActionResult UpdateAccountProcessVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);

            AccountProcessVariable variableToChange = _varDB.AccountProcessVariables.Find(updates.PKeyId);

            variableToChange.VariableData = updates.Data;
            variableToChange.User = User.Identity.Name;

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed " + updates.Name + " to " + updates.Data + ".", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]        
        public ActionResult AddAccountProcessVariables(string values)
        {
            UpdateVariableObjList updates = new JavaScriptSerializer().Deserialize<UpdateVariableObjList>(values);

            try
            {
                foreach (var update in updates.data)
                {
                    _varDB.AccountProcessVariables.Add(new AccountProcessVariable()
                    {
                        ServerAccountId = update.ServerAccountId,
                        ProcessId = update.ProcessId,
                        VariableId = update.VariableId,
                        VariableData = update.Data,
                        User = User.Identity.Name
                    });
                }
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Process", "Controller");

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProcessVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]        
        public ActionResult UpdateAccountProcessVariables(string values)
        {
            UpdateVariableObjList updates = new JavaScriptSerializer().Deserialize<UpdateVariableObjList>(values);

            List<int> variableIds = new List<int>();
            foreach (var update in updates.data)
            {
                variableIds.Add(update.PKeyId);
            }

            List<AccountProcessVariable> variableToChange = (from apv in _varDB.AccountProcessVariables
                                                             where variableIds.Contains(apv.AccountProcessVariableId)
                                                             select apv).OrderBy(apvl => apvl.AccountProcessVariableId).ToList();

            List<UpdateVariableObj> updatesList = updates.data.OrderBy(ul => ul.PKeyId).ToList();

            for (int n = 0; n < updatesList.Count; n++)
            {
                variableToChange[n].VariableData = updatesList[n].Data;
                variableToChange[n].User = User.Identity.Name;
            }

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed.", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed: " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }

}