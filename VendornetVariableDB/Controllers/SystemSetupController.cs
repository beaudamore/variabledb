﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VendornetVariableDB.Models;
using System.Web.Script.Serialization;
using PagedList;

using VendornetVariableDB.ViewModels;

namespace VendornetVariableDB.Controllers
{
    [Authorize]
    public class SystemSetupController : Controller
    {
        private ExtVariablesEntities _varDB = new ExtVariablesEntities();

        public ActionResult Index()
        {
            return View();
        }

        #region Servers
        public ActionResult Servers()
        {
            List<Server> currentServers = (from s in _varDB.Servers select s).ToList();

            ViewData["currentServers"] = currentServers;

            return View();
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult AddServer(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            try
            {
                Server dataToAdd = new Server()
                {
                    ServerName = updates.Name
                };

                _varDB.Servers.Add(dataToAdd);
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Server", "Controller");

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult DeleteServer(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);
            Server delThis = _varDB.Servers.Find(updates.ServerId);

            try
            {
                _varDB.Servers.Remove(delThis);
                _varDB.SaveChanges();
                return Json("Successfully deleted " + updates.Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult UpdateServer(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            Server variableToChange = (from s in _varDB.Servers
                                       where s.ServerId == updates.ServerId
                                       select s).FirstOrDefault();

            variableToChange.ServerName = updates.Name;

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed to " + updates.Name + ".", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ClientAccountTypes
        public ActionResult ClientAccountTypes()
        {
            List<AccountType> currentAccountTypes = (from at in _varDB.AccountTypes select at).ToList();

            ViewData["currentAccountTypes"] = currentAccountTypes;

            return View();
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult AddClientAccountType(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            try
            {
                AccountType dataToAdd = new AccountType()
                {
                    AccountTypeDesc = updates.Name
                };

                _varDB.AccountTypes.Add(dataToAdd);
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Server", "Controller");

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult DeleteClientAccountType(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);
            AccountType delThis = _varDB.AccountTypes.Find(updates.PKeyId);

            try
            {
                _varDB.AccountTypes.Remove(delThis);
                _varDB.SaveChanges();
                return Json("Successfully deleted " + updates.Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult UpdateClientAccountType(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            AccountType variableToChange = (from at in _varDB.AccountTypes
                                            where at.AccountType1 == updates.PKeyId
                                            select at).FirstOrDefault();

            variableToChange.AccountTypeDesc = updates.Name;

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed to " + updates.Name + ".", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ClientAccounts
        public ActionResult ClientAccounts()
        {
            List<v_Accounts> currentAccounts = (from va in _varDB.v_Accounts select va).ToList();
            List<AccountType> accountTypes = (from at in _varDB.AccountTypes select at).ToList();

            ViewData["currentAccounts"] = currentAccounts;
            ViewData["accountTypes"] = accountTypes;

            return View();
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult AddClientAccount(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            try
            {
                Account dataToAdd = new Account()
                {
                    AccountName = updates.Name,
                    AccountType = updates.AccountTypeId
                };

                _varDB.Accounts.Add(dataToAdd);
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Server", "Controller");

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult DeleteClientAccount(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);
            Account delThis = _varDB.Accounts.Find(updates.AccountId);

            try
            {
                _varDB.Accounts.Remove(delThis);
                _varDB.SaveChanges();
                return Json("Successfully deleted " + updates.Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateClientAccount(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            Account variableToChange = (from a in _varDB.Accounts
                                        where a.AccountId == updates.AccountId
                                        select a).FirstOrDefault();

            variableToChange.AccountName = updates.Name;
            variableToChange.AccountType = updates.AccountTypeId;

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed " + updates.Name + ".", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Programs
        public ActionResult Programs()
        {
            List<Program> currentPrograms = (from p in _varDB.Programs select p).ToList();

            ViewData["currentPrograms"] = currentPrograms;

            return View();
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult AddProgram(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            try
            {
                Program dataToAdd = new Program()
                {
                    ProgramName = updates.Name,
                    Owner = updates.Owner,
                    Active = updates.Active,
                    ProgramDescription = updates.Description,
                    ExeName = updates.ExeName,
                    TFSPath = updates.TFSPath
                };

                _varDB.Programs.Add(dataToAdd);
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Server", "Controller");

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult DeleteProgram(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);
            Program delThis = _varDB.Programs.Find(updates.ProgramId);

            try
            {
                _varDB.Programs.Remove(delThis);
                _varDB.SaveChanges();
                return Json("Successfully deleted " + updates.Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult UpdateProgram(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            Program variableToChange = (from p in _varDB.Programs
                                        where p.ProgramId == updates.ProgramId
                                        select p).FirstOrDefault();

            variableToChange.ProgramName = updates.Name;
            variableToChange.Owner = updates.Owner;
            variableToChange.Active = updates.Active;
            variableToChange.ProgramDescription = updates.Description;
            variableToChange.ExeName = updates.ExeName;
            variableToChange.TFSPath = updates.TFSPath;

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed to " + updates.Name + ".", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Processes
        public ActionResult Processes()
        {
            List<Process> currentProcesses = (from pp in _varDB.Processes select pp).ToList();
            List<Program> programs = (from p in _varDB.Programs select p).ToList();

            ViewData["currentProcesses"] = currentProcesses;
            ViewData["programs"] = programs;

            return View();
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult AddProcess(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            try
            {
                Process dataToAdd = new Process()
                {
                    ProgramId = updates.ProgramId,
                    ProcessName = updates.Name,
                    ProcessDescription = updates.Description,
                    Owner = updates.Owner,
                    Active = updates.Active
                };

                _varDB.Processes.Add(dataToAdd);
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Server", "Controller");

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult DeleteProcess(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);
            Process delThis = _varDB.Processes.Find(updates.ProcessId);

            try
            {
                _varDB.Processes.Remove(delThis);
                _varDB.SaveChanges();
                return Json("Successfully deleted " + updates.Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult UpdateProcess(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            Process variableToChange = (from pp in _varDB.Processes
                                        where pp.ProcessId == updates.ProcessId
                                        select pp).FirstOrDefault();

            variableToChange.ProgramId = updates.ProgramId;
            variableToChange.ProcessName = updates.Name;
            variableToChange.ProcessDescription = updates.Description;
            variableToChange.Owner = updates.Owner;
            variableToChange.Active = updates.Active;

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed " + updates.Name + ".", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Variables
        // GET: Server
        public ActionResult Variables(string search, string currentFilter, int? page)
        {
            List<Variable> variables;

            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentFilter;
            }

            ViewBag.CurrentFilter = search;

            if (!String.IsNullOrEmpty(search))
            {
                variables = (from v in _varDB.Variables where v.VariableName.Contains(search) select v).ToList();
            }
            else
            {
                variables = (from v in _varDB.Variables select v).ToList();
            }

            variables = variables.OrderByDescending(x => x.SortIndex).ToList();

            ViewData["Variables"] = variables.OrderByDescending(x => x.SortIndex).ToList(); // use SortIndex

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(variables.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult AddNewVariable(VariableToAddViewModel model)
        {
            // setup return model
            JsonResultViewModel returnModel = new JsonResultViewModel()
            {
                IsSuccess = true
            };
            // try to add
            try
            {
                Variable newVariable = new Variable()
                {
                    VariableName = model.VariableName,
                    SortIndex = model.SortIndex,
                    VariableDescription = model.VariableDescription,
                    DefaultLevel = model.DefaultLevel.HasValue ? Convert.ToByte(model.DefaultLevel) : Convert.ToByte(null),
                    Server = model.IsServer,
                    Account = model.IsAccount,
                    Program = model.IsProgram,
                    Process = model.IsProcess
                };
                _varDB.Variables.Add(newVariable);
                _varDB.SaveChanges();
            }
            catch (Exception ex)
            {
                // log error somehow.?

                // graceful user feedback
                returnModel.IsSuccess = false;
                returnModel.Message = ex.Message;
            }        
            // return the returnModel 
            return Json(returnModel);
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult DeleteVariable(string id)
        {
            // setup return model
            JsonResultViewModel returnModel = new JsonResultViewModel()
            {
                IsSuccess = true
            };
            // try to add
            try
            {
                int idInt;
                var didConvertToInt = int.TryParse(id, out idInt);
                Variable variableToDelete = _varDB.Variables.Find(idInt);
                _varDB.Variables.Remove(variableToDelete);
                _varDB.SaveChanges();
            }
            catch (Exception ex)
            {
                // get meaningful error message
                if (ex.InnerException.InnerException != null)
                {
                    returnModel.Message = ex.InnerException.InnerException.ToString();
                }
                else
                {
                    returnModel.Message = ex.Message;
                }
                // graceful user feedback
                returnModel.IsSuccess = false;
            }
            // return the returnModel 
            return Json(returnModel);
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult UpdateVariable(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            Variable variableToChange = (from apv in _varDB.Variables
                                         where apv.VariableId == updates.PKeyId
                                         select apv).FirstOrDefault();

            variableToChange.VariableName = updates.Name;
            variableToChange.SortIndex = updates.SortIndex;
            variableToChange.VariableDescription = updates.Description;
            variableToChange.Server = updates.AvailServer;
            variableToChange.Account = updates.AvailAccount;
            variableToChange.Program = updates.AvailProgram;
            variableToChange.Process = updates.AvailProcess;
            variableToChange.DefaultLevel = updates.DefLevel;

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed " + updates.Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed on " + updates.Name + " : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin")]
        public ActionResult UpdateVariables(string values)
        {
            UpdateSetupObjList updates = new JavaScriptSerializer().Deserialize<UpdateSetupObjList>(values);

            List<int> variableIds = new List<int>();
            foreach (var update in updates.data)
            {
                variableIds.Add(update.PKeyId);
            }

            List<Variable> variableToChange = (from apv in _varDB.Variables
                                               where variableIds.Contains(apv.VariableId)
                                               select apv).OrderBy(apvl => apvl.VariableId).ToList();

            List<UpdateSetupObj> updatesList = updates.data.OrderBy(ul => ul.PKeyId).ToList();

            for (int n = 0; n < updatesList.Count; n++)
            {
                variableToChange[n].VariableName = updatesList[n].Name;
                variableToChange[n].SortIndex = updatesList[n].SortIndex;
                variableToChange[n].VariableDescription = updatesList[n].Description;
                variableToChange[n].Server = updatesList[n].AvailServer;
                variableToChange[n].Account = updatesList[n].AvailAccount;
                variableToChange[n].Program = updatesList[n].AvailProgram;
                variableToChange[n].Process = updatesList[n].AvailProcess;
                variableToChange[n].DefaultLevel = updatesList[n].DefLevel;
            }

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed.", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed: " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}