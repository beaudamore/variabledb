﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VendornetVariableDB.Models;
using System.Web.Script.Serialization;
using VendornetVariableDB.ViewModels;
using VendornetVariableDB.Classes;

namespace VendornetVariableDB.Controllers
{
    [Authorize]
    public class ClientAccountController : Controller
    {
        private ExtVariablesEntities _varDB = new ExtVariablesEntities();

        public ActionResult Index(int serverId)
        {
            // get account info
            List<GetAccountsAvailability_Result> cilentAccounts = (from gaa in _varDB.GetAccountsAvailability(serverId) select gaa).ToList();
            List<GetAccountsAvailability_Result> serverAccounts = new List<GetAccountsAvailability_Result>();
            List<GetAccountsAvailability_Result> availableAccounts = new List<GetAccountsAvailability_Result>();
            // sort accounts
            foreach (GetAccountsAvailability_Result clientAccount in cilentAccounts)
            {
                if (clientAccount.ServerAccountId == null)
                {
                    availableAccounts.Add(clientAccount);
                }
                else
                {
                    serverAccounts.Add(clientAccount);
                }
            }

            // fill returnModel
            var returnModel = new ClientAccountIndexViewModel()
            {
                // tierinfo
                TierInfo = new TierInfo()
                {
                    ServerId = serverId,
                    ServerName = (from s in _varDB.Servers where s.ServerId == serverId select s.ServerName).First().ToString(),
                },
                // accounts
                AvailableAccounts = availableAccounts,
                ServerAccounts = serverAccounts
            };           

            return View(returnModel);
        }

        public ActionResult Modify(int serverAccountId)
        {
            List<GetAccountVariables_Result> clientAccountVariables = (from apv in _varDB.GetAccountVariables(serverAccountId) select apv).ToList();

            var replacements = (from r in _varDB.FindReplaces
                                where r.Find.Contains("Server") || r.Find.Contains("Account")
                                select new { r.Find, r.ReplaceWith }).ToList();

            List<GetAccountVariables_Result> currentVars = new List<GetAccountVariables_Result>();
            List<GetAccountVariables_Result> addRecommendedVars = new List<GetAccountVariables_Result>();
            List<GetAccountVariables_Result> addNonRecommendedVars = new List<GetAccountVariables_Result>();

            foreach (GetAccountVariables_Result clientAccountVariable in clientAccountVariables)
            {
                if (clientAccountVariable.AccountVariableId != null)
                {
                    // AccountAccountVariables.AccountAccountVariableId is the primary key and it has a value; this value is already set and can be modified
                    currentVars.Add(clientAccountVariable);
                }
                else
                {
                    // AccountAccountVariables.AccountAccountVariableId is the primary key and it is null;
                    //    this variable does not exist at this level for this server/account/Account/Account
                    if (clientAccountVariable.DefaultLevel == (byte)Enums.VariableLevelEnum.Account)
                    {
                        // DefaultLevel 2 means this variable is recommended to be set at the Account level
                        addRecommendedVars.Add(clientAccountVariable);
                    }
                    else
                    {
                        // It is not recommended that this variable be set at the Account level; however, it is allowed to be set at that level
                        addNonRecommendedVars.Add(clientAccountVariable);
                    }
                }
            }

            List<FindReplaceModel> findReplacements = new List<FindReplaceModel>();

            foreach (var replacement in replacements)
            {
                findReplacements.Add(new FindReplaceModel(replacement.Find, replacement.ReplaceWith));
            }

            // does not work, even when using public class AvailableVariables : IEquatable<GetAccountVariables_Result>
            //availableVariables = availableVariables.RemoveAll(item => accountProgramVariables.Contains(item));

            // possible to do these below in 1 command? same record in table (ServerAccounts)
            int serverId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.ServerId).First();
            int accountId = (from sa in _varDB.ServerAccounts where sa.ServerAccountId == serverAccountId select sa.AccountId).First();


            TierInfo tierInfo = new TierInfo
            {
                ServerAccountId = serverAccountId,
                ServerId = serverId,
                ServerName = clientAccountVariables.First().ServerName,
                AccountId = accountId,
                AccountName = clientAccountVariables.First().AccountName,
            };

            AccountModifyViewModel model = new AccountModifyViewModel()
            {
                TierInfo = tierInfo,
                CurrentVariables = currentVars,
                AddRecommendedVars = addRecommendedVars,
                AddNonRecommendedVars = addNonRecommendedVars,
                Replacements = findReplacements
            };
            
            return View(model);
        }

        [Authorize(Roles = "Admin,User")]
        public ActionResult AddAccount(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            try
            {
                ServerAccount dataToAdd = new ServerAccount()
                {
                    ServerId = updates.ServerId,
                    AccountId = updates.AccountId
                };

                _varDB.ServerAccounts.Add(dataToAdd);
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Server", "Controller");

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult RemoveAccount(string values)
        {
            UpdateSetupObj updates = new JavaScriptSerializer().Deserialize<UpdateSetupObj>(values);

            ServerAccount delThis = _varDB.ServerAccounts.Find(updates.PKeyId);

            try
            {
                _varDB.ServerAccounts.Remove(delThis);
                _varDB.SaveChanges();
                return Json("Successfully deleted ServerAccount " + updates.PKeyId.ToString(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }
       
        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult AddAccountVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);

            try
            {
                AccountVariable dataToAdd = new AccountVariable()
                {
                    ServerAccountId = updates.ServerAccountId,
                    VariableId = updates.VariableId,
                    VariableData = updates.Data,
                    User = User.Identity.Name
                };

                _varDB.AccountVariables.Add(dataToAdd);
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Account", "Controller");

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult DeleteAccountVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);
            AccountVariable delThis = _varDB.AccountVariables.Find(updates.PKeyId);

            try
            {
                _varDB.AccountVariables.Remove(delThis);
                _varDB.SaveChanges();
                return Json("Successfully deleted " + updates.Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult UpdateAccountVariable(string values)
        {
            UpdateVariableObj updates = new JavaScriptSerializer().Deserialize<UpdateVariableObj>(values);

            AccountVariable variableToChange = _varDB.AccountVariables.Find(updates.PKeyId);

            variableToChange.VariableData = updates.Data;
            variableToChange.User = User.Identity.Name;

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed " + updates.Name + " to " + updates.Data + ".", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult AddAccountVariables(string values)
        {
            UpdateVariableObjList updates = new JavaScriptSerializer().Deserialize<UpdateVariableObjList>(values);

            try
            {
                foreach (var update in updates.data)
                {
                    _varDB.AccountVariables.Add(new AccountVariable()
                    {
                        ServerAccountId = update.ServerAccountId,
                        VariableId = update.VariableId,
                        VariableData = update.Data,
                        User = User.Identity.Name
                    });
                }
                _varDB.SaveChanges();

                var redirectUrl = new UrlHelper(Request.RequestContext).Action("Account", "Controller");
                //ModelState.Clear();
                //return View(new Modify)

                // "Successfully added " + updates.Name + ": " + updates.Data + " to AccountProgramVariables."
                return Json(redirectUrl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed : " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, Authorize(Roles = "Admin,User")]
        public ActionResult UpdateAccountVariables(string values)
        {
            UpdateVariableObjList updates = new JavaScriptSerializer().Deserialize<UpdateVariableObjList>(values);

            List<int> variableIds = new List<int>();
            foreach (var update in updates.data)
            {
                variableIds.Add(update.PKeyId);
            }

            List<AccountVariable> variableToChange = (from apv in _varDB.AccountVariables
                                                      where variableIds.Contains(apv.AccountVariableId)
                                                      select apv).OrderBy(apvl => apvl.AccountVariableId).ToList();

            List<UpdateVariableObj> updatesList = updates.data.OrderBy(ul => ul.PKeyId).ToList();

            for (int n = 0; n < updatesList.Count; n++)
            {
                variableToChange[n].VariableData = updatesList[n].Data;
                variableToChange[n].User = User.Identity.Name;
            }

            try
            {
                _varDB.SaveChanges();
                return Json("Successfully changed.", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Json("Change Failed: " + e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}