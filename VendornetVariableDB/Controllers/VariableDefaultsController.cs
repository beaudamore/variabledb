﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using VendornetVariableDB.Models;
using VendornetVariableDB.ViewModels;

namespace VendornetVariableDB.Controllers
{
    [Authorize]
    public class VariableDefaultsController : Controller
    {
        private ExtVariablesEntities db = new ExtVariablesEntities();

        // GET: Variables
        public ActionResult Index()
        {
            VariableIndexViewModel model = new VariableIndexViewModel()
            {
                AvailableVariables =
                    db.v_Defaults
                        .OrderBy(z => z.VariableName)
                        .Select(x => new DDLWithDescriptionViewModel { Id = x.VariableId.ToString(),
                                                                       Name = x.VariableName,
                                                                       Description = x.VariableDescription })
                        .Distinct(),
                    //db.v_Defaults
                    //    //.Where(z=>//z.VariableId == 40 
                    //    //   //&& (z.ThisLevel == z.DefaultLevel || z.ProgramId == 3) // this is at account level
                    //    //   //&& 
                    //    //   //z.AccountType == 1
                    //    //   )
                    //    .OrderBy(y=>y.VariableName)
                    //    .Select(x => new SelectListItem {Text = x.VariableName, Value = x.VariableId.ToString()})
                    //    .Distinct(),

                 AccountTypes =
                    db.AccountTypes
                        //.Where(z => z.VariableId == 40
                        //   //&& (z.ThisLevel == z.DefaultLevel || z.ProgramId == 3) // this is at account level
                        //   && z.AccountType == 1)
                        .OrderBy(y => y.AccountTypeDesc)
                        .Select(x => new SelectListItem { Text = x.AccountTypeDesc, Value = x.AccountType1.ToString() })
                        .Distinct()                        
            }; 
            return View(model);
        }        

        public PartialViewResult GetVariablesToEdit(VariableGetVariablesToEditViewModel modelIn)
        {
            var testObj = db.v_Defaults
                        .Where(z => z.VariableId == modelIn.VariableTypeId
                           && z.AccountType == modelIn.AccountTypeId);

            VariableGetVariablesToEditViewModel model = new VariableGetVariablesToEditViewModel()
            {
                VariablesToEdit = testObj
                    .Select(x => new VariableToEditViewModel
                    {
                        VariableId = x.VariableId ?? 0,
                        VariableName = x.VariableName,
                        VariableDescription = x.VariableDescription,
                        Account = x.Account,
                        DefaultLevel = x.DefaultLevel,
                        DefaultValue = x.DefaultValue,
                        LevelId = x.ThisLevel,
                        ProgramId = x.ProgramId.Value.ToString() ?? string.Empty,
                        ProgramName = x.ProgramName,
                        ProcessId = x.ProcessId.Value.ToString() ?? string.Empty,
                        ProcessName = x.ProcessName,
                        Server = x.Server,
                        ThisLevel = x.ThisLevel
                    })
                    .ToList(),
                VariableTypeId = modelIn.VariableTypeId,
                AccountTypeId = modelIn.AccountTypeId
            };

            return PartialView("_VariableGetVariablesToEdit", model);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult UpdateVariable(VariableUpdateViewModel model)
        {
            // setup json return model
            JsonResultViewModel returnModel = new JsonResultViewModel();
                
            try
            {
                // variableDefault to update
                var variableDefaultToEdit = db.VariableDefaults
                                         .FirstOrDefault(
                                                x => x.VariableId == model.VariableId
                                                      && x.Server == model.Server
                                                      && x.Account == model.Account
                                                      && x.AccountType == model.AccountType
                                                      && x.ProcessId == model.ProcessId
                                                      && x.ProgramId == model.ProgramId
                                                      );
                // if exists, update
                if (variableDefaultToEdit != null)
                {
                    variableDefaultToEdit.DefaultValue = model.DefaultValue;
                    // tell repo
                    db.Entry(variableDefaultToEdit).State = EntityState.Modified;
                }
                else
                {
                    // create new
                    var varDefaultNew = new VariableDefault()
                    {
                        Account = model.Account,
                        AccountType = byte.Parse(model.AccountType.ToString()),
                        DefaultValue = model.DefaultValue,
                        ProcessId = model.ProcessId,
                        ProgramId = model.ProgramId,
                        Server = model.Server,
                        VariableId = model.VariableId
                    };
                    db.VariableDefaults.Add(varDefaultNew);
                }
                // save changes
                db.SaveChanges();

                // return model
                returnModel = new JsonResultViewModel()
                {
                    IsSuccess = true,
                    Message = string.Empty
                };
            }
            catch(Exception ex)
            {
                returnModel.IsSuccess = false;
                returnModel.Message = ex.Message;
                return Json(returnModel, JsonRequestBehavior.AllowGet);
                //throw ex;
            }
            // return for ajax
            return Json(returnModel, JsonRequestBehavior.AllowGet);
        }

        // GET: Variables/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            v_Defaults v_Defaults = db.v_Defaults.Find(id);
            if (v_Defaults == null)
            {
                return HttpNotFound();
            }
            return View(v_Defaults);
        }

        // GET: Variables/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Variables/Create
        [HttpPost, Authorize(Roles = "Admin"), ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DefaultId,VariableId,Server,Account,ProgramId,ProcessId,AccountType,AccountTypeDesc,VariableName,ProgramName,ProcessName,DefaultValue,ThisLevel,DefaultLevel")] v_Defaults v_Defaults)
        {
            if (ModelState.IsValid)
            {
                db.v_Defaults.Add(v_Defaults);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(v_Defaults);
        }

        // GET: Variables/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            v_Defaults v_Defaults = db.v_Defaults.Find(id);
            if (v_Defaults == null)
            {
                return HttpNotFound();
            }
            return View(v_Defaults);
        }

        // POST: Variables/Edit/5
        [HttpPost, Authorize(Roles = "Admin"), ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DefaultId,VariableId,Server,Account,ProgramId,ProcessId,AccountType,AccountTypeDesc,VariableName,ProgramName,ProcessName,DefaultValue,ThisLevel,DefaultLevel")] v_Defaults v_Defaults)
        {
            if (ModelState.IsValid)
            {
                db.Entry(v_Defaults).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(v_Defaults);
        }

        // GET: Variables/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            v_Defaults v_Defaults = db.v_Defaults.Find(id);
            if (v_Defaults == null)
            {
                return HttpNotFound();
            }
            return View(v_Defaults);
        }

        // POST: Variables/Delete/5
        [HttpPost, ActionName("Delete"), Authorize(Roles = "Admin"), ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            v_Defaults v_Defaults = db.v_Defaults.Find(id);
            db.v_Defaults.Remove(v_Defaults);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
