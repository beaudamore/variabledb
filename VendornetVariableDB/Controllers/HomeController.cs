﻿using System.Web.Mvc;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.Controllers
{
    // [Authorize]
    [RequireHttps]
    public class HomeController : Controller
    {
        private ExtVariablesEntities _varDB = new ExtVariablesEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About(string accountId)
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        

    }
}