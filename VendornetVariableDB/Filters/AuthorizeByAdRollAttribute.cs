﻿using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.Filters
{
    // TODO: unfinished
    public class AuthorizeByAdRoleAttribute : AuthorizeAttribute
    {
        // use this attribute like this:
        //[AuthorizeByAdRole(Groups = "Admin,ImpDev")]
        // !!--making sure any string group name definitely has an AD Group Name to match--!!

        private ExtVariablesEntities _varDB = new ExtVariablesEntities();
  
        public AuthorizeByAdRoleAttribute()
        {

        }

        /// <summary>
        /// this comes from decoration like this: [AuthorizeByRole(Groups = "Admin,ImpDev")]
        /// </summary>
        public string Groups { get; set; }
        
        /// <summary>
        /// Override the authorization routine to check if this user is part of 'AllowedOUs' (web.config key; comma delimited)
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext)) return false;

            // Get the groups for this authorization
            var groups = Groups.Split(',').ToList();

            // Verify that the user is in the given AD group (if any)
            var context = new PrincipalContext(
                ContextType.Domain,
                WebConfigurationManager.AppSettings["AllowedDomain"]);

            var userPrincipal = UserPrincipal.FindByIdentity(
             context,
             IdentityType.SamAccountName,
             httpContext.User.Identity.Name);
            
            // determine if the user is allowed to see this controller/page
            foreach (var group in groups)
            {
                if (userPrincipal.IsMemberOf(context,
                    IdentityType.Name,
                    @group))
                { return true; }
            }

            return false;
        }

        /// <summary>
        /// redirect on failure
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(
        AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                var result = new ViewResult
                {
                    ViewName = "_NotAuthorized",
                    MasterName = "_Layout"
                };
                filterContext.Result = result;
            }
            else
                base.HandleUnauthorizedRequest(filterContext);
        }
    }
}
