﻿using System.Web.Mvc;
using System.Web.Routing;
using VendornetVariableDB.Classes;

namespace VendornetVariableDB.Filters
{
    internal class RallyCredsValidateFilter : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            //var aDum = new AdUserManagement();
            if (!AdUserManagement.IsUserSetup())
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {   // TODO: change where this goes
                    { "Controller", "UserProfile" },
                    { "Action", "MyRallyCreds" }
                });

            }
        }
    }
}
