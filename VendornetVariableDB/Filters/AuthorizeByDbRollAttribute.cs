﻿using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.Filters
{
    public class AuthorizeByDbRoleAttribute : AuthorizeAttribute
    {
        // use this attribute like this:
        //[AuthorizeByDbRole(Groups = "Admin,ImpDev,ImpEng,Basic")]
        // !!--making sure any string group name definitely has a Db entry to match--!!

        //private readonly ExtVariablesEntities _varDB = new ExtVariablesEntities();
  
        public AuthorizeByDbRoleAttribute()
        {

        }

        /// <summary>
        /// this comes from decoration like this: [AuthorizeByRole(Groups = "Admin,ImpDev")]
        /// </summary>
        public string Groups { get; set; }
        
        /// <summary>
        /// Override the authorization routine to check if this user is part of 'AllowedOUs' (web.config key; comma delimited)
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext)) return false;

            // Get the groups for this authorization (from the decoration attribute)
            var groups = Groups.Split(',').ToList();

            var userDisplayName = string.Empty;
            var userSid = string.Empty;

            using (HostingEnvironment.Impersonate())
            {
                // Verify that the user is in the given AD group (if any)
                var context = new PrincipalContext(
                    ContextType.Domain);

                var userPrincipal = UserPrincipal.FindByIdentity(
                    context,
                    IdentityType.SamAccountName,
                    httpContext.User.Identity.Name);

                //userDisplayName = userPrincipal.DisplayName;
                userSid = userPrincipal.Sid.ToString();
            }

            using (ExtVariablesEntities _varDB = new ExtVariablesEntities())
            {
                // check for UserRoles for this 
                var user = _varDB.Users.FirstOrDefault(x => x.Sid == userSid);
                
                IList<UserRole> usersRoles = new List<UserRole>();
                if (user != null)
                {
                    usersRoles = user.UserRoles.ToList();
                }

                // determine if the user is allowed to see this controller/page
                foreach (var ur in usersRoles)
                {
                    if (groups.Contains(ur.RoleName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// redirect on failure
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(
        AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                var result = new ViewResult
                {
                    ViewName = "_NotAuthorized",
                    MasterName = "_Layout"
                };
                filterContext.Result = result;
            }
            else
                base.HandleUnauthorizedRequest(filterContext);
        }
    }
}
