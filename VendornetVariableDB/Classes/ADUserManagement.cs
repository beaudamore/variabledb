﻿
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Configuration;
using System.Linq;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.Classes
{
    public class AdUserManagement: Controller
    {
        // repos for this controller - make sure to dispose
        //private readonly IUserProfileRepository _userProfileRepository; 
        private ExtVariablesEntities _varDB = new ExtVariablesEntities();
        // --

        public AdUserManagement() // initialize the repositories for this controller
        {
            //this._userProfileRepository = new UserProfileRepository(new RallyContext());
        }

        /// <summary>
        /// Check on SessionStart if this user has Rally Creds, 
        /// If not, redirect to force creds entry by user
        /// </summary>
        /// <returns></returns>
        public static bool IsUserSetup()
        {
            using (HostingEnvironment.Impersonate())
            {
                var userContext = System.Web.HttpContext.Current.User.Identity;
                var ctx = new PrincipalContext(ContextType.Domain, ConfigurationManager.AppSettings["AllowedDomain"], null,
                    ContextOptions.Negotiate | ContextOptions.SecureSocketLayer);
                var user = UserPrincipal.FindByIdentity(ctx, userContext.Name);

                // check if this user exists in AD
                if (user == null)
                {
                    return false;
                }

                // check if this user exists in Db with roles
                using (ExtVariablesEntities _varDB = new ExtVariablesEntities())
                {
                    var u = _varDB.Users.FirstOrDefault(x => x.Sid == user.Sid.Value.ToString());
                    if (u != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Get a list of the AD Users in the specified groups from the web.config keys
        /// that are NOT already in the Db
        /// Used in:
        /// - UserProfile/CreateUserProfile
        /// </summary>
        /// <param name="domainName"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetListOfAdUsersByGroupNotInDb(string domainName, string groupName)
        {
            // return var
            List<SelectListItem> ddlToReturn = new List<SelectListItem>();

            // create list of groups to search through from web.config app setting
            List<string> groupsToCheck = ConfigurationManager.AppSettings["AllowedOUs"].Split('|').ToList();

            using (HostingEnvironment.Impersonate())
            {
                using (var context = new PrincipalContext(ContextType.Domain)) // TODO: can modify this line to specify a particular domain and/or loop through domain in pipe delimited web.config list
                {
                    // loop through each group to gather users
                    foreach (var group in groupsToCheck)
                    {
                        using (var currentGroup = GroupPrincipal.FindByIdentity(context, group))
                        {
                            if (currentGroup == null) continue;
                            var users = currentGroup.GetMembers(true); // recursively enumerate
                            foreach (var user in users)
                            {
                                // TODO: might need to remove this one check if we want a ddl for editing any/all users and/or autocomplete
                                // only add those who aren't already added
                                var userSid = user.Sid.ToString();
                                var userToTest = _varDB.Users.FirstOrDefault(x => x.Sid == userSid);
                                if (userToTest == null)
                                {
                                    ddlToReturn.Add(new SelectListItem
                                    {
                                        Value = user.Sid.ToString(),
                                        Text = user.DisplayName
                                    });
                                }
                            }
                        }
                    }
                }
            }
            return ddlToReturn;           
        }

        /// <summary>
        /// Get all OUs in respective domain
        /// Used mainly for interrogating AD to figure out what groups/OUs are available.
        /// </summary>
        public static void GetAllOUs()
        {
            // this gets all OU's in a domain
            List<string> orgUnits = new List<string>();
            DirectoryEntry startingPoint = new DirectoryEntry("LDAP://DIRECTNET");
            DirectorySearcher searcher = new DirectorySearcher(startingPoint);
            searcher.Filter = "(objectCategory=organizationalUnit)";
            foreach (SearchResult res in searcher.FindAll())
            {
                orgUnits.Add(res.Path);
            }
        }

        // -- end of class
    }



    // AD MANAGEMENT METHODOLOGIES
    // ---------------------------

    // --one way--
    // set up domain context
    //PrincipalContext ctx = new PrincipalContext(ContextType.Domain);
    // find a user
    //UserPrincipal user = UserPrincipal.FindByIdentity(ctx, "SomeUserName");

    // --another way--
    //DirectoryEntry entry = new DirectoryEntry("LDAP://" + domainName); //+ ",DC=com");
    //DirectorySearcher search = new DirectorySearcher(entry);
    //string query = "(&(objectCategory=person)(objectClass=user)(memberOf=*))";
    //search.Filter = query;
    //search.PropertiesToLoad.Add("memberOf");
    //search.PropertiesToLoad.Add("name");
    //search.PropertiesToLoad.Add("userPrincipalName");

    //System.DirectoryServices.SearchResultCollection mySearchResultColl = search.FindAll();            

    //foreach (SearchResult result in mySearchResultColl)
    //{
    //    //result.Properties.PropertyNames
    //    foreach (string prop in result.Properties["memberOf"])
    //    {
    //        if (prop.Contains(groupName))
    //        {
    //            returnDictionary.Add(result.Properties["name"][0].ToString(), result.Properties["userPrincipalName"][0].ToString());
    //        }
    //    }
    //}

}
