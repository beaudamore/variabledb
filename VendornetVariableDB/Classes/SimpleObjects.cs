﻿using System.Web.Hosting;

namespace VendornetVariableDB.Classes
{
    public static class SimpleObjects
    {
     
        public static string GetUserFullName()
        {
            // find currently logged in user
            using (HostingEnvironment.Impersonate())
            {
                using (
                    var context =
                        new System.DirectoryServices.AccountManagement.PrincipalContext(
                            System.DirectoryServices.AccountManagement.ContextType.Domain))
                {
                    var principal = System.DirectoryServices.AccountManagement.UserPrincipal.FindByIdentity(
                        context, System.Web.HttpContext.Current.User.Identity.Name);
                    //string.Concat(principal.GivenName, " ", principal.Surname);
                    return principal.DisplayName;
                }
            }
        }
    }
}
