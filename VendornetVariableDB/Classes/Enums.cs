﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendornetVariableDB.Classes
{
    public class Enums
    {
        public enum VariableLevelEnum : byte
        {
            None = 0,
            Server = 1,
            Account = 2,
            Program = 3,
            Process = 4
        }


    }
}
