﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VendornetVariableDB.Startup))]
namespace VendornetVariableDB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
