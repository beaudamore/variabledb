﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace VendornetVariableDB.Services
{
    class EmailService : SmtpClient
    {
        // Gmail user-name
        //public string UserName { get; set; }

        public EmailService():
        base( ConfigurationManager.AppSettings["SmtpHost"], Int32.Parse(ConfigurationManager.AppSettings["SmtpPort"]) )
    {
            //Get values from web.config file:
            //this.UserName = ConfigurationManager.AppSettings["GmailUserName"];
            this.EnableSsl = false;
            this.UseDefaultCredentials = true;
            //this.Host = ConfigurationManager.AppSettings["SmtpUrl"];
            //this.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            //this.Credentials = new System.Net.NetworkCredential("corp\bdamore", "2003Mu5t@ng");
        }

    }
}
