﻿
using System.Collections;

namespace VendornetVariableDB.ViewModels
{
    public class JsonResultViewModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

    }
}
