﻿using System.Collections.Generic;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ProcessModifyViewModel
    {
        public TierInfo TierInfo { get; set; }
     
        public List<GetProcessVariables_Result> CurrentVariables { get; set; }
     
        public List<GetProcessVariables_Result> AddRecommendedVars { get; set; }
       
        public List<GetProcessVariables_Result> AddNonRecommendedVars { get; set; }

        public List<FindReplaceModel> Replacements { get; set; }        
    }
}
