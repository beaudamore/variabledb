﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace VendornetVariableDB.ViewModels
{
    public class VariableListToEditViewModel
    {
        public int VarID { get; set; }

        public List<string> VarList  { get; set; }
    }
}
