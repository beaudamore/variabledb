﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace VendornetVariableDB.ViewModels
{
    public class VariableIndexViewModel
    {
        //public IEnumerable<SelectListItem> AvailableVariables { get; set; }
        public IEnumerable<DDLWithDescriptionViewModel> AvailableVariables { get; set; }
        public IEnumerable<SelectListItem> AccountTypes { get; set; }

    }
}
