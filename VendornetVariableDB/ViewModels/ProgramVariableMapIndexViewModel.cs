﻿
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ProgramVariableMapIndexViewModel
    {
        public IEnumerable<SelectListItem> Programs { get; set; }
        public IEnumerable<SelectListItem> Processes { get; set; }

    }
}
