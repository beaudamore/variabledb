﻿using System.Collections.Generic;

namespace VendornetVariableDB.ViewModels
{
    public class VariableGetVariablesToEditViewModel
    {
        public int VariableTypeId { get; set; }

        public int AccountTypeId { get; set; }

        //public System.Linq.IGrouping<int?, VariableToEditViewModel> VariablesToEdit { get; set; }

        public IEnumerable<VariableToEditViewModel> VariablesToEdit { get; set; }

        //public IEnumerable<IGrouping<string, VariableToEditViewModel>> VariablesToEdit { get; set; }

        //public IEnumerable<VariableListToEditViewModel> VariablesToEdit { get; set; }


    }
}
