﻿
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ProgramIndexViewModel
    {
        public TierInfo TierInfo { get; set; }
        public IEnumerable<Program> AllPrograms { get; set; }

    }
}
