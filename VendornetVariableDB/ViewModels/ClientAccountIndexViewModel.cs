﻿
using System.Collections;
using System.Collections.Generic;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ClientAccountIndexViewModel
    {
        public List<GetAccountsAvailability_Result> AvailableAccounts { get; set; }
        public List<GetAccountsAvailability_Result> ServerAccounts { get; set; }
        public TierInfo TierInfo { get; set; }

    }
}
