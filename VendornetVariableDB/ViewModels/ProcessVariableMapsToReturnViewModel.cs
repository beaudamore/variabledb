﻿using System.Collections.Generic;
using System.Web.Mvc;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ProcessVariableMapsToReturnViewModel
    {
        public IEnumerable<SelectListItem> AvailableVariables { get; set; }
              
        public IEnumerable<ProcessVariableMap> ExistingProcessVariableMaps { get; set; }
  
    }
}
