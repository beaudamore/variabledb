﻿
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;

namespace VendornetVariableDB.ViewModels
{
    public class VariableUpdateViewModel
    {
        public int VariableId { get; set; }
        public bool Server { get; set; }
        public bool Account { get; set; }
        public int AccountType { get; set; }
        public int? ProgramId { get; set; }
        public int? ProcessId { get; set; }

        // and the value from the form
        public string DefaultValue { get; set; }

    }
}
