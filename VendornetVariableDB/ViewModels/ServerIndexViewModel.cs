﻿using System.Collections.Generic;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ServerIndexViewModel
    {
        public TierInfo TierInfo { get; set; }
     
        public List<Server> AllServers { get; set; }
     
    }
}
