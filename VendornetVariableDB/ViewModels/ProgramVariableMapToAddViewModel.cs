﻿
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ProgramVariableMapToAddViewModel
    {
        public int ProgramId { get; set; }

        public IEnumerable<VariableViewModel> AssignedVariables { get; set; }

    }
}
