﻿
using System.Collections;

namespace VendornetVariableDB.ViewModels
{
    public class UserRoleViewModel
    {
        public string Name { get; set; }
        public bool IsChecked { get; set; }

    }
}
