﻿
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;

namespace VendornetVariableDB.ViewModels
{
    public class DDLWithDescriptionViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
