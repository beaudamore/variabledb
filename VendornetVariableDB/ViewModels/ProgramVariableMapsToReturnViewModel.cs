﻿using System.Collections.Generic;
using System.Web.Mvc;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ProgramVariableMapsToReturnViewModel
    {
        public IEnumerable<SelectListItem> AvailableVariables { get; set; }

        public IEnumerable<ProgramVariableMap> ExistingProgramVariableMaps { get; set; }

    }
}
