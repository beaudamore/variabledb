﻿using System.Collections.Generic;

namespace VendornetVariableDB.ViewModels
{
    public class ProcessVariableMapToAddViewModel
    {
        public int ProcessId { get; set; }

        public IEnumerable<VariableViewModel> AssignedVariables { get; set; }
        
    }
}
