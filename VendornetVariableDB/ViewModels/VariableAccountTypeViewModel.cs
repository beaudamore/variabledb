﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace VendornetVariableDB.ViewModels
{
    public class VariableAccountTypeViewModel
    {
       public IEnumerable<SelectListItem> AccountTypes { get; set; }

    }
}
