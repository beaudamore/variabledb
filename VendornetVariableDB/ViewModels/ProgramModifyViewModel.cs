﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ProgramModifyViewModel
    {
        public TierInfo TierInfo { get; set; }
     
        public List<GetProgramVariables_Result> CurrentVariables { get; set; }
     
        public List<GetProgramVariables_Result> AddRecommendedVars { get; set; }
       
        public List<GetProgramVariables_Result> AddNonRecommendedVars { get; set; }

        public List<FindReplaceModel> Replacements { get; set; }        
    }
}
