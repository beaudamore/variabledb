﻿using System.Collections.Generic;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ServerModifyViewModel
    {
        public TierInfo TierInfo { get; set; }
     
        public List<GetServerVariables_Result> CurrentVariables { get; set; }
     
        public List<GetServerVariables_Result> AddRecommendedVars { get; set; }
       
        public List<GetServerVariables_Result> AddNonRecommendedVars { get; set; }

        public List<FindReplaceModel> Replacements { get; set; }        
    }
}
