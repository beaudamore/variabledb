﻿
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class ProgramVariableMixedResultViewModel
    {
        public bool IsSuccess { get; set; }
        public int ReturnType { get; set; }
        public string ReturnHtml { get; set; }
        public IEnumerable<SelectListItem> ReturnDDLOptions { get; set; }

    }
}
