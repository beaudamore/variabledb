﻿using System.ComponentModel.DataAnnotations;

namespace VendornetVariableDB.ViewModels
{
    public class VariableToAddViewModel
    {
        public string VariableName { get; set; }
        public int SortIndex { get; set; }

        public string VariableDescription { get; set; }

        public int? DefaultLevel { get; set; }
             
        public bool? IsServer { get; set; }
        public bool? IsAccount { get; set; }
        public bool? IsProgram { get; set; }
        public bool? IsProcess { get; set; }

     
    }
}
