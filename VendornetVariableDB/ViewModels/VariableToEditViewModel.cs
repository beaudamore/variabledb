﻿using System.ComponentModel.DataAnnotations;

namespace VendornetVariableDB.ViewModels
{
    public class VariableToEditViewModel
    {
        [Key]
        public int VariableId { get; set; }

        public bool? Account { get; set; }

        public byte? AccountType { get; set; }

        public string AccountTypeDesc { get; set; }


        public int? DefaultId { get; set; }

        public int? DefaultLevel { get; set; }

        public string DefaultValue { get; set; }

     
     
        public string ProcessId { get; set; }

        public string ProcessName { get; set; }


        public string ProgramId { get; set; }

        public string ProgramName { get; set; }


        public bool? Server{ get; set; }

        public int ThisLevel { get; set; }

        public int LevelId { get; set; }

        public string VariableName { get; set; }

        public string VariableDescription { get; set; }


    }
}
