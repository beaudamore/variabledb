﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace VendornetVariableDB.ViewModels
{
    public class UserNewViewModel
    {
        public string Name { get; set; }

        public string Sid { get; set; }

        [DisplayName("User Roles")]
        public IEnumerable<UserRoleViewModel> UserRoles { get; set; }

        public IEnumerable<SelectListItem> AvailableAdUsers { get; set; }
   
    }
}
