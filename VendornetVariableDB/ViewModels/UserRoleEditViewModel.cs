﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class UserRoleEditViewModel
    {

        [DisplayName("User Id")]
        public int UserId { get; set; }

        [DisplayName("User Sid")]
        public string UserSid { get; set; }

        [DisplayName("User Display Name")]
        public string UserDisplayName { get; set; }

        [DisplayName("User Roles")]
        public IEnumerable<UserRoleViewModel> UserRoles { get; set; }
       
    }

}
