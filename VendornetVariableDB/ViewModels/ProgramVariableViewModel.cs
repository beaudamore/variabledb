﻿using System.Collections.Generic;

namespace VendornetVariableDB.ViewModels
{
    public class VariableViewModel
    {
        public string VariableId { get; set; }
        public string IsRequired { get; set; }
        
    }
}
