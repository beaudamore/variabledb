﻿
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace VendornetVariableDB.ViewModels
{
    public class VariableMapsToGetViewModel
    {
        [Key]
        public string ProgramId { get; set; }
        [Key]
        public string ProcessId { get; set; }

    }
}
