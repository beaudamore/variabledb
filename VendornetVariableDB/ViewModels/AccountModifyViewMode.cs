﻿using System.Collections.Generic;
using VendornetVariableDB.Models;

namespace VendornetVariableDB.ViewModels
{
    public class AccountModifyViewModel
    {
        public TierInfo TierInfo { get; set; }
     
        public List<GetAccountVariables_Result> CurrentVariables { get; set; }
     
        public List<GetAccountVariables_Result> AddRecommendedVars { get; set; }
       
        public List<GetAccountVariables_Result> AddNonRecommendedVars { get; set; }

        public List<FindReplaceModel> Replacements { get; set; }        
    }
}
